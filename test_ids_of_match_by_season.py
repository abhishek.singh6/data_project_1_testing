import unittest
from ids_of_match_by_season import get_ids

class GetIds(unittest.TestCase):

    def test_get_ids(self):
        expected_data = ['91', '92']
        actual_data   = get_ids(2008)
        self.assertEqual(expected_data, actual_data)
        
      
