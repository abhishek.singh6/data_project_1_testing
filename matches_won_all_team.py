import csv
import operator

def matches_won(file_name):
    try:
        with open(file_name, newline ='') as csv_file:
            matches_reader = csv.DictReader(csv_file)
            matches_won_per_team_per_season = {}
            total_teams = []
            for match in matches_reader:
                if match['winner'] != '':
                    if match['season'] not in matches_won_per_team_per_season:
                        matches_won_per_team_per_season[match['season']] = match['winner']
                    else:
                        matches_won_per_team_per_season[match['season']] = match['winner'] +","+matches_won_per_team_per_season[match['season']]
                    if match['winner'] not in total_teams:
                        total_teams.append(match['winner']) 
        return matches_won_per_team_per_season, total_teams   
    except FileNotFoundError:
        return "Unable to locate file, File not founded"
    except OSError:
        return "OS error found"
    except ValueError:
        return "Value is not valid, Value Error"
    except KeyError:
        return "Key Error Founded"

      
def get_per_year_team_win_count(matches_won_per_team_per_season, total_teams):          
    total_teams.sort()
    per_year_team_win_count = {}
    try:
        for year in matches_won_per_team_per_season:
            total_win_team = matches_won_per_team_per_season[year].split(',')
            team_and_count = {}
            total_team_played = []
            for team in total_win_team:
                if team not in team_and_count:
                    team_and_count[team] = 1
                    total_team_played.append(team)
                else:
                    team_and_count[team] += 1
            not_played_team = list(set(total_teams)-set(total_team_played))
            val_not_played_by_team = {}
            for team in not_played_team:
                val_not_played_by_team[team] = 0
            team_and_count.update(val_not_played_by_team)
            sorted_team_value = sorted(team_and_count.items(), key = operator.itemgetter(0))
            per_year_team_win_count[year] = sorted_team_value
        return per_year_team_win_count, total_teams
    except ValueError:
        return "Value Error"
    except TypeError:
        return "Type Error"
    except KeyError:
        return "Key Error Founded"

def execute(file_path):
    matches_won_per_team_per_season, total_teams = matches_won(file_path)
    per_year_team_win_count, total_teams = get_per_year_team_win_count(matches_won_per_team_per_season, total_teams)  
    return per_year_team_win_count, total_teams  
