from csv import DictReader
import ids_of_match_by_season
from operator import add
import operator

def get_scorer(file_path, ids_of_match_by_season):
    batsman = {}
    try:
        with open(file_path, 'rt') as csv_file:
            deliveries = DictReader(csv_file)
            for delivery in deliveries:
                if delivery['match_id'] in ids_of_match_by_season:
                    if delivery['batsman'] not in batsman.keys():
                        batsman[delivery['batsman']] = int(delivery['batsman_runs'])
                    else:
                        batsman[delivery['batsman']] += int(delivery['batsman_runs'])
        return batsman    
    except FileNotFoundError:
       return "Unable to locate file, File not founded"
    except OSError:
        return "OS error occurs"
    except ValueError:
        return "Value is not valid, Value Error"
    except TypeError:
        return "Input data type is not valid "
    except KeyError:
        return "Key Error Founded"            
                    
def get_top_scorer(batsman):   
    top_scores = []
    top_scorer = []
    try:
        sorted_batsman = sorted(batsman.items(), key = operator.itemgetter(1))[:10][::-1]
        for i in range(len(sorted_batsman)):
            top_scorer.append(sorted_batsman[i][0])
            top_scores.append(sorted_batsman[i][1])
        return top_scorer, top_scores
    except ValueError:
        return "Value is not valid, Value Error"
    except TypeError:
        return "Input data type is not valid "
    
def execute_top_scorer(file_path):
    season = str(2016)
    batsman = get_scorer(file_path, ids_of_match_by_season.get_ids(season))
    top_scorer, top_scores = get_top_scorer(batsman)
    result = {}
    for i in range(len(top_scorer)):
        result[top_scorer[i]] = top_scores[i]
    return result    

