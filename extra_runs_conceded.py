from csv import DictReader
import ids_of_match_by_season

def get_extra_runs_per_teams(file_path, ids_of_match_by_season):
    teams = {}
    try:
        with open(file_path,'rt')as csv_file:
            deliveries_reader = DictReader(csv_file)
            for delivery in deliveries_reader:
                if delivery['match_id'] in ids_of_match_by_season:
                    if delivery['bowling_team'] not in teams.keys():
                        teams[delivery['bowling_team']] = int(delivery['extra_runs'])
                    else:
                        teams[delivery['bowling_team']] = int(delivery['extra_runs'])+ int(teams[delivery['bowling_team']])
        extra_runs_per_teams = [extra_runs for extra_runs in teams.values()]
        teams_list = [team for team in teams.keys()]
        return extra_runs_per_teams, teams_list
    except FileNotFoundError:
       return "Unable to locate file, File not founded"
    except OSError:
        return "OS error occurs"
    except ValueError:
        return "Value is not valid, Value Error"
    except KeyError:
        return "Key Error Founded"

def get_extra_runs_per_teams_execute(file_path):
    ids = ids_of_match_by_season.get_ids(2016)
    extra_runs_per_teams, teams_list = get_extra_runs_per_teams(file_path, ids)
    return extra_runs_per_teams,teams_list
