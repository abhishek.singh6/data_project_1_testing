import unittest
from matches_played_per_season import matches_per_season

class TestMatchesPlayedPerSeason(unittest.TestCase):

    def test_matches_played_season(self):
            
        expected_data = {'2008': 9}
        actual_data   = matches_per_season("matches2008.csv")
        self.assertEqual(expected_data, actual_data)
        
        self.assertEqual(matches_per_season("matches2009.csv"), {'2009': 6})
        self.assertEqual(matches_per_season("matches2010.csv"), {'2010': 24})
        self.assertEqual(matches_per_season("matches2011.csv"), {'2011': 3})
        self.assertEqual(matches_per_season("matches2012.csv"), {'2012': 5})
        self.assertEqual(matches_per_season("matches2013.csv"), {'2013': 10})
        self.assertEqual(matches_per_season("matches2014.csv"), {})
        self.assertEqual(matches_per_season("matches2015.csv"), {'2015': 4})
        self.assertEqual(matches_per_season("matches2016.csv"), {'2016': 38})
        self.assertEqual(matches_per_season("matches2017.csv"), {'2017': 4})
