import unittest
from matches_won_all_team import execute

class TestExtraRunsPerTeams(unittest.TestCase):

    def test_execute(self):
        expected_data =({'2017': [('Chennai Super Kings', 0), ('Kolkata Knight Riders', 1), ('Rising Pune Supergiant', 1), ('Royal Challengers Bangalore', 0)], '2008': [('Chennai Super Kings', 1), ('Kolkata Knight Riders', 1), ('Rising Pune Supergiant', 0), ('Royal Challengers Bangalore', 0)], '2010': [('Chennai Super Kings', 1), ('Kolkata Knight Riders', 0), ('Rising Pune Supergiant', 0), ('Royal Challengers Bangalore', 1)]}, ['Chennai Super Kings', 'Kolkata Knight Riders', 'Rising Pune Supergiant', 'Royal Challengers Bangalore'])
        actual_data = execute("matchestest.csv")
        self.assertEqual(actual_data, expected_data)
        
        