import unittest
from extra_runs_conceded import get_extra_runs_per_teams_execute

class TestExtraRunsPerTeams(unittest.TestCase):
    
    def test_get_extra_runs_per_teams_execute(self):
        expected_data = ([7, 4, 3], ['Gujarat Lions', 'Kolkata Knight Riders', 'Royal Challengers Bangalore'])
        actual_data = get_extra_runs_per_teams_execute("deliveries2016.csv")
        self.assertEqual(actual_data, expected_data)
        
