from csv import DictReader

def matches_per_season(file_path):
    matches_count_dict = {}
    try:
        with open(file_path, 'rt') as csv_file:
            matches = DictReader(csv_file)
            for match in matches:
                if match['season'] not in matches_count_dict.keys():
                    matches_count_dict[match['season']] = 1  
                else:     
                    matches_count_dict[match['season']] += 1
        return matches_count_dict
    
    except FileNotFoundError:
       return "Unable to locate file, File not founded"
    except OSError:
        return "OS error occurs"
    except KeyError:
        return "Key Error Founded"
    except ValueError:
        return "Value is not valid, Value Error"
    
    
