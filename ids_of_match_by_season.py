from csv import DictReader

def get_ids(season):
    ids_of_matches = []
    try:
        season = str(season)
        with open('matches.csv', 'rt') as csv_file:
            matches_reader = DictReader(csv_file)
            for match in matches_reader:
                if match['season'] == season:
                    ids_of_matches.append(match['id'])
        return ids_of_matches
    except FileNotFoundError:
       return "Unable to locate file, File not founded"
    except OSError:
        return "OS error occurs"
    except ValueError:
        return "Value is not valid, Value Error"
    except KeyError:
        return "Key Error Founded"
    

