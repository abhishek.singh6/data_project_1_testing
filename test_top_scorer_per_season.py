import unittest
# from  top_scorer_per_season import execute_top_scorer
import top_scorer_per_season

class TestTopScorerPerSeason(unittest.TestCase):

    def test_execute_top_scorer(self):
        expected_data = {'M Vijay': 9, 'AM Rahane': 8, 'DA Warner': 5, 'Q de Kock': 5, 'SV Samson': 2, 'S Dhawan': 1, 'MP Stoinis': 0, 'KK Nair': 0, 'SS Iyer': 0, 'SS Tiwary': 0}
        actual_data   = top_scorer_per_season.execute_top_scorer("deliveries2016.csv")
        print(actual_data)
        self.assertEqual(expected_data,actual_data)
        
        
if __name__ == "__main__":
    unittest.main()