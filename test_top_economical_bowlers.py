import unittest
from top_economical_bowlers import execute_most_economical_bowlers

class ExecuteMostEconomicalBowlers(unittest.TestCase):

    def test_execute_most_economical_bowlers(self):
        expected_output = {'AD Russell': 3.0, 'UT Yadav': 4.0, 'SP Narine': 5.0, 'M Morkel': 6.0, 'Shakib Al Hasan': 7.714}    
        self.assertEqual(execute_most_economical_bowlers("deliveries2015.csv"),expected_output)
        
        
       
