from csv import DictReader
import ids_of_match_by_season

def get_economical_bowlers(file_path, ids_of_match_by_season):
    bowlers = {}
    try:
        with open(file_path, 'rt') as csv_file:
            deliveries = DictReader(csv_file)
            for delivery in deliveries:
                if delivery['match_id'] in ids_of_match_by_season:
                    if delivery['bowler'] not in bowlers.keys():
                        bowlers[delivery['bowler']] = {}
                        bowlers[delivery['bowler']]['ball'] = 1
                        bowlers[delivery['bowler']]['run'] = int(delivery['total_runs'])
                    else:
                        bowlers[delivery['bowler']]['ball'] += 1
                        bowlers[delivery['bowler']]['run'] += int(delivery['total_runs'])
        return bowlers
    except FileNotFoundError:
       return "Unable to locate file, File not founded"
    except OSError:
        return "OS error occurs"
    except ValueError:
        return "Value is not valid, Value Error"
    except TypeError:
        return "Input data type is not valid "
    except KeyError:
        return "Key Error Founded"
                
def get_most_economical_bowlers(bowlers):           
    bowlers_economy = {}
    economy_list = []
    try:
        for key, value in bowlers.items():
            bowlers_economy[key] = round(((value['run']/value['ball'])*6), 3)
            economy_list.append(round(((value['run']/value['ball'])*6), 3))
        most_economical_list = sorted(economy_list)[:5]
        most_economical_bowlers = []
        for i in range(len(most_economical_list)):
            most_economical_bowlers.append(list(bowlers_economy.keys())[list(bowlers_economy.values()).index(most_economical_list[i])])
        return most_economical_bowlers, most_economical_list
    except ValueError:
        return "Value is not valid, Value Error"
    except TypeError:
        return "Input data type is not valid "

def execute_most_economical_bowlers(file_path):
    economical_bowlers = get_economical_bowlers(file_path, ids_of_match_by_season.get_ids(2015))
    most_economical_bowlers, most_economical_list = get_most_economical_bowlers(economical_bowlers)
    result = {}
    for i in range(len(most_economical_bowlers)):
        result[most_economical_bowlers[i]] = most_economical_list[i]
        
    return result
    
     